## Top dir 
### Structure files
`POSCAR`, `structure.inc`, `PROPhet.xml`

These files are the same structure of 640 Si atoms for AMP, LAMMPS, and PROPhet runs.

### Input files
`run_amp.py`, `lammps.in`, `input_prophet`

### Scripts
`test_Rc.py`: Generate input for each `Rc`

`get_data.py`: Collect energies

## In each Rc-XX folder
### Output files (data)
`AMP-ENERGY.dat`, `log.lammps`, `out_prophet`

## Results

```
   Rc     diff          AMP      PROPhet
 3.00  0.02750  -2592.16260  -2592.19010
 3.50  0.00411  -2595.01558  -2595.01969
 4.00  0.00710  -2598.63670  -2598.64380
 4.50  0.10183  -2605.00613  -2605.10797
 5.00  0.22921  -2617.09499  -2617.32419
 5.50  0.31322  -2639.20679  -2639.52000
 6.00  0.37036  -2679.67094  -2680.04130
 6.50  0.67169  -2771.48909  -2772.16078
 7.00  1.60704  -3017.68116  -3019.28821
 7.50  0.59668  -3264.70767  -3265.30435
```

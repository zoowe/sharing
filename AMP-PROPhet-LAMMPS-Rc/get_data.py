from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import os

def get_amp( f_ ):
    with open( f_, 'r' ) as f:
        return float( f.readline().split()[1] )

def get_prophet( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'System' in lines[ i ]:
            return float( lines[ i + 2 ] .split( )[ 1 ] )

def get_lammps( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'Step' in lines[ i ]:  
            return float( lines[ i + 1 ] .split( )[ 1 ] ) 
    
     
print "%5s %8s %12s %12s" %( 'Rc', 'diff', 'AMP', 'PROPhet' )
for Rc in np.arange(3.0, 8.0, 0.5 ):
    folder    ='Rc-' + str( Rc )
    e_amp     = get_amp    ( folder + '/AMP_ENERGY.DAT')
    e_prophet = get_prophet( folder + '/out_prophet'   ) 
    e_lammps  = get_lammps ( folder + '/log.lammps'    ) 
    print "%5.2f %8.5f %12.5f %12.5f" %( Rc, e_amp - e_prophet, e_amp, e_prophet)

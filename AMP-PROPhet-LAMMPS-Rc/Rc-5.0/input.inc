# Network details
# ------------------------------------------------
network_type = nn        # Perform a fit using a neural network
hidden=  12 1   # 2 hidden layers of 10 nodes
input= structure         # Use the atomic configuration as input
output = energy          # Use total energy as output
transfer = tanh          # Use a tanh transfer funtion for all nodes


# Specify the geometry mapping functions
# ------------------------------------------------
Rcut=8.0
G2 8.0 0 0.0208333333333 0
G2 8.0 0 0.0416666666667 0
G2 8.0 0 0.0625 0
G2 8.0 0 0.0833333333333 0
G2 8.0 0 0.104166666667 0
G2 8.0 0 0.125 0
G3 8.0 0 0.0015625 1 1
G3 8.0 0 0.0015625 1 -1
G3 8.0 0 0.0015625 8 1
G3 8.0 0 0.0015625 8 -1
G3 8.0 0 0.0015625 16 1
G3 8.0 0 0.0015625 16 -1


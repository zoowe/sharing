from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import os

for Rc in np.arange(3.0, 8.0, 0.5 ):

    folder='Rc-' + str( Rc )
    os.system( 'mkdir -p ' + folder )
    os.system( 'cat amp.amp | sed \'s/$Rc/' + str(Rc) + '/\' > ' + folder + '/amp.amp')
    os.chdir( folder )
    calc = Amp.load('amp.amp')

    save_to_prophet( calc, filename='potential_', overwrite=True, units="metal")
    os.system( 'sbatch ../job')
    os.chdir( '../' )

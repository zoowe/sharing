from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import os

for eta in np.arange(1, 20, 1):
  for zeta in np.arange(1,20, 1):
    folder='eta-' + str( eta )+'_zeta' + str( zeta )
    os.system( 'mkdir -p ' + folder )
    os.system( 'cat amp.amp | sed \'s/$eta/' + str(eta) + '/\' | sed \'s/$zeta/' + str(zeta) + '/\' > ' + folder + '/amp.amp')
    os.chdir( folder )
    calc = Amp.load('amp.amp')

    save_to_prophet( calc, filename='potential_', overwrite=True, units="metal")
    os.system( 'sbatch ../job')
    os.chdir( '../' )

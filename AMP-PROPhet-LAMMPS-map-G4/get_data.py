from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import os

def get_amp( f_ ):
    with open( f_, 'r' ) as f:
        return float( f.readline().split()[1] )

def get_prophet( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'System' in lines[ i ]:
            return float( lines[ i + 2 ] .split( )[ 1 ] )

def get_lammps( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'Step' in lines[ i ]:  
            return float( lines[ i + 1 ] .split( )[ 1 ] ) 
    
     
#print "%5s %8s %12s %12s" %( 'eta', 'diff', 'AMP', 'PROPhet' )
def get_ediff( folder ):
    e_amp     = get_amp    ( folder + '/AMP_ENERGY.DAT')
    e_prophet = get_prophet( folder + '/out_prophet'   ) 
    e_lammps  = get_lammps ( folder + '/log.lammps'    ) 
    e_diff = e_prophet - e_amp
    return e_diff

data = np.zeros( [19, 19 ] )
for eta in np.arange(0, 19, 1):
  for zeta in np.arange(0,19, 1):
      folder='eta-' + str( eta + 1)+'_zeta' + str( zeta + 1)
      data[ eta, zeta ] = get_ediff( folder ) 

## Top dir 
### Structure files
`POSCAR`, `structure.inc`, `PROPhet.xml`

These files are the same structure of 640 Si atoms for AMP, LAMMPS, and PROPhet runs.

### Input files
`run_amp.py`, `lammps.in`, `input_prophet`

### Scripts
`test_eta_zeta.py`: Generate input for `eta` and `zeta`
`get_data.py`: Collect energies

## In each `eta-XX_zetaYY` folder
### Output files (data)
`AMP-ENERGY.dat`, `log.lammps`, `out_prophet`

## Results
Difference between AMP and PROPhet cause by G4 as a function of `eta` and `zeta`.

![image](Error_G4.png)


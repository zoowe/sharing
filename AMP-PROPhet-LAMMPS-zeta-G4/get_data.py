from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import os

def get_amp( f_ ):
    with open( f_, 'r' ) as f:
        return float( f.readline().split()[1] )

def get_prophet( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'System' in lines[ i ]:
            return float( lines[ i + 2 ] .split( )[ 1 ] )

def get_lammps( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'Step' in lines[ i ]:  
            return float( lines[ i + 1 ] .split( )[ 1 ] ) 
    
     
#print "%5s %8s %12s %12s" %( 'zeta', 'diff', 'AMP', 'PROPhet' )
def get_ediff( loc ):
    e_diff = {}
    list_dir = os.listdir( loc )
    for item in list_dir:
        if 'zeta-' in item and os.path.isdir( loc + '/' + item ):
            zeta = item.replace( 'zeta-', '')
            folder= loc + '/' + item 
            e_amp     = get_amp    ( folder + '/AMP_ENERGY.DAT')
            e_prophet = get_prophet( folder + '/out_prophet'   ) 
            e_lammps  = get_lammps ( folder + '/log.lammps'    ) 
            e_diff[ float(zeta) ] = e_prophet - e_amp
    return e_diff

list_topdir = os.listdir( '.' )
data = {}
for item in list_topdir:
    if 'Rc-' in item and os.path.isdir( item ):
        Rc = item.replace( 'Rc-', '' )
        data[ float( Rc ) ] = get_ediff( item )

#print sorted( data.keys() )

x = sorted( data[ data.keys()[0]].keys() )

for Rc in sorted( data.keys( ) ):
    color = cm.jet( int( ( Rc - 4 ) / 4. * 255 ) )
    y = np.zeros( [ len ( x ) ] )
    i = 0
    for zeta in sorted( data[ Rc ].keys( ) ):
         y [ i ] = data[ Rc ][ zeta ]
         i += 1
    plt.plot(x, y, '-o', c = color, label = str( Rc ) )
     

#plt.ylim( -5, 5 )
plt.xlim( 1, 16 )
plt.legend( loc =1 )
plt.xlabel( r'$\zeta$' )
plt.ylabel( r'$E_{AMP} - E_{PROPhet}$' )
plt.ylabel( r'$E_{AMP} - E_{PROPhet}$ [eV]' )
plt.title( r'G$_4$' )

plt.savefig( 'G4_zeta.png', dpi= 300, bbox_inches='tight' )



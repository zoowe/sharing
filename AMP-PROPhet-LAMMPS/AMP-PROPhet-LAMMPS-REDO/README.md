Same as in ../. However, in this test, large `eta` are used. `zeta` are 1, 8.5, 16

### Structure files
`POSCAR`, `structure.inc`, `PROPhet.xml`

These files are the same structure of 640 Si atoms for AMP, LAMMPS, and PROPhet runs.

### Input files
`run_amp.py`, `lammps.in`, `input_prophet`

`convert.py`: convert `amp.amp` to `potential_Si`

### Output files (data)
`AMP-ENERGY.dat`, `log.lammps`, `out_prophet`

### Energy

AMP:     -2537.35038323 

LAMMPS:  -2537.349952 

PROPhet: -2537.34995198 


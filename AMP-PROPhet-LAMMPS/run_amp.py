import ase.io
from amp import Amp

calc = Amp.load('../amp.amp')

system = ase.io.read( 'POSCAR' )
with open( 'AMP_ENERGY.DAT', 'w') as f:
    print >>f, 'AMP: ', calc.get_potential_energy( system )

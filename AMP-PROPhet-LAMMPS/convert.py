from amp import Amp
from amp.convert import save_to_prophet

calc = Amp.load('amp.amp')

save_to_prophet( calc, filename='potential_', overwrite=True, units="metal")

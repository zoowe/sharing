### Structure files
`POSCAR`, `structure.inc`, `PROPhet.xml`

These files are the same structure of 640 Si atoms for AMP, LAMMPS, and PROPhet runs.

### Input files
`run_amp.py`, `lammps.in`, `input_prophet`

`convert.py`: convert `amp.amp` to `potential_Si`

### Output files (data)
`AMP-ENERGY.dat`, `log.lammps`, `out_prophet`

### Energy

AMP:     -3315.2287823

LAMMPS:  -3267.0295 

PROPhet: -3267.029577 


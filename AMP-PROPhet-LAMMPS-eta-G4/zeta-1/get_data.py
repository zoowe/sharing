from amp import Amp
from amp.convert import save_to_prophet
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import os

def get_amp( f_ ):
    with open( f_, 'r' ) as f:
        return float( f.readline().split()[1] )

def get_prophet( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'System' in lines[ i ]:
            return float( lines[ i + 2 ] .split( )[ 1 ] )

def get_lammps( f_ ):
    with open( f_, 'r' ) as f:
        lines = f.readlines()
    for i in range( len( lines ) ):
        if 'Step' in lines[ i ]:  
            return float( lines[ i + 1 ] .split( )[ 1 ] ) 
    
     
#print "%5s %8s %12s %12s" %( 'eta', 'diff', 'AMP', 'PROPhet' )
def get_ediff( loc ):
    e_diff = {}
    list_dir = os.listdir( loc )
    for item in list_dir:
        if 'eta-' in item and os.path.isdir( loc + '/' + item ):
            eta = item.replace( 'eta-', '')
            folder= loc + '/' + item 
            e_amp     = get_amp    ( folder + '/AMP_ENERGY.DAT')
            e_prophet = get_prophet( folder + '/out_prophet'   ) 
            e_lammps  = get_lammps ( folder + '/log.lammps'    ) 
            e_diff[ float(eta) ] = e_prophet - e_amp
    return e_diff

list_topdir = os.listdir( '.' )
data = {}
for item in list_topdir:
    if 'Rc-' in item and os.path.isdir( item ):
        Rc = item.replace( 'Rc-', '' )
        data[ float( Rc ) ] = get_ediff( item )

#print sorted( data.keys() )

x = sorted( data[ data.keys()[0]].keys() )

for Rc in sorted( data.keys( ) ):
    color = cm.jet( int( ( Rc - 4 ) / 4. * 255 ) )
    y = np.zeros( [ len ( x ) ] )
    i = 0
    for eta in sorted( data[ Rc ].keys( ) ):
         y [ i ] = data[ Rc ][ eta ]
         i += 1
    plt.plot(x, y, '-o', c = color, label = str( Rc ) )
     

plt.ylim( -5, 1 )
plt.xlim( 1, 20 )
plt.legend( loc = 4 )
plt.xlabel( r'$\eta$' )
plt.ylabel( r'$E_{AMP} - E_{PROPhet}$' )
plt.ylabel( r'$E_{AMP} - E_{PROPhet}$ [eV]' )
plt.title( r'G$_4$ $\zeta=1$' )

plt.savefig( 'G4_eta-1.png', dpi= 300, bbox_inches='tight' )



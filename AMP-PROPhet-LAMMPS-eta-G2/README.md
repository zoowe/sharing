### Structure files
`POSCAR`, `structure.inc`, `PROPhet.xml`

These files are the same structure of 640 Si atoms for AMP, LAMMPS, and PROPhet runs.

### Input files
`run_amp.py`, `lammps.in`, `input_prophet`

### Scripts
`test_Rc.py`: Generate input for each `Rc`

`get_data.py`: Collect energies

## In each Rc-XX folder
### Output files (data)
`AMP-ENERGY.dat`, `log.lammps`, `out_prophet`

## Results
This shows the differences cause by G2 for various Rc (see legend).  
![image](G2_eta.png)
